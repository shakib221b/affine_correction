import numpy as np
import cv2


def show(img, name):
    """Shows an image and waits for key press

    Arguments:
        img {numpy.ndarray}
        name {str}
    """
    cv2.imshow(name, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def draw_circle(image, pts):
    """Draws small circle in the point co-ordinates
    """    
    for cx, cy in pts:
        cv2.circle(image, (cx, cy), 1, (0, 255, 0), -1)
    show(image, f"{len(pts)}_points")


def order_points(points):
    """Orders points from top-left, top-right, bottom-right, bottom-left
    """    
    points = np.array(points)
    rect = np.zeros((4, 2), dtype="float32")
    s = np.sum(points, axis=1)

    rect[0] = points[np.argmin(s)]  # top-left point
    rect[2] = points[np.argmax(s)]  # bottom-right point

    diff = np.diff(points, axis=1)
    rect[1] = points[np.argmin(diff)]   # top-right point
    rect[3] = points[np.argmax(diff)]   # bottom-left point
    # return the ordered coordinates
    return rect


def euclidean_distance(point1, point2):
    return (((point1[0] - point2[0]) ** 2) + ((point1[1] - point2[1]) ** 2)) ** 0.5


def four_point_transform(image, points):
    """Perform cv2 perspective transform

    Arguments:
        image {numpy.ndarray} -- image to be transformed
        points {numpy.ndarray} -- 2d contour points

    Returns:
        numpy.ndarray -- transformed image
    """
    assert (points.ndim == 2), "points array have to be two dimensional"
    rect = order_points(points)
    # draw_circle(image.copy(), rect)
    (tl, tr, br, bl) = rect

    maxWidth = int(max(euclidean_distance(br, bl), euclidean_distance(tr, tl)))
    maxHeight = int(max(euclidean_distance(tr, br), euclidean_distance(tl, bl)))
    dst = np.array(
        [
            [0, 0],
            [maxWidth, 0],
            [maxWidth, maxHeight],
            [0, maxHeight]
        ],
        dtype="float32"
    )
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    # return the warped image
    return warped


def get_contour_outline(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.bilateralFilter(gray, 11, 17, 17)
    # show(gray, "gray")
    gray = cv2.medianBlur(gray, 5)
    # show(gray, "blur")
    edged = cv2.Canny(gray, 10, 100)
    # show(edged, "edged1")
    kernel = np.ones((5, 5), np.uint8)
    dilated = cv2.dilate(edged, kernel, iterations=2)
    # show(dilated, "dilated")

    # we should not use cv2.CHAIN_APPROX_SIMPLE,
    # the option reduces points and it leads insufficient affine corrections.
    cnts, _ = cv2.findContours(
        dilated.copy(),
        cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_NONE
    )
    assert (len(cnts) != 0), "No of contours is zero!"

    def perimeter(k):
        return cv2.arcLength(k, False)

    c = max(cnts, key=cv2.contourArea)
    # c = max(cnts, key=lambda k : perimeter(k))
    # convert to 2d
    c = np.squeeze(c, axis=1)
    return c


def affine_correction(image):
    """Perform affine correction
    """
    c = get_contour_outline(image)
    return four_point_transform(image, c)
