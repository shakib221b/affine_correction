import cv2
import numpy as np
import json
from utils import show, affine_correction
from argparse import ArgumentParser


def get_args():
    parser = ArgumentParser()
    parser.add_argument("-i", "--image", required=True)
    parser.add_argument("-a", "--annos", required=True)
    parser.add_argument("--basic-affine", default=True)
    parser.add_argument("--part-affine", default=False)
    return parser.parse_args()


def main():
    args = get_args()
    img = cv2.imread(args.image)

    with open(args.annos) as f:
        annos = json.load(f)

    objects = annos["objects"]
    for obj in objects:
        if obj["classTitle"] != "Name Plate":
            continue
        if len(obj["tags"]) != 0:
            # It's unclear
            continue
        bbox = obj["points"]["exterior"]

        # Make cv2 contour-like shape
        bbox = np.array(bbox, dtype="int32")
        bbox = np.expand_dims(bbox, 1)

        x, y, w, h = cv2.boundingRect(bbox)
        # Draw rectangle on the ROI of image
        rect = cv2.rectangle(img.copy(), (x, y), (x+w, y+h), (0, 0, 255), 2)
        show(rect, "rect")
        padding = 2
        crop_img = img[y-padding:y+h+padding, x-padding:x+w+padding]
        show(crop_img, "crop_img")

        if args.basic_affine:
            print("performing basic affine correction")
            show(affine_correction(crop_img), "Basic_affine_corrected_image")

        if args.part_affine:
            print("performing partitioned affine correction")
            show(partitioned_affine(crop_img), "Part_affine_corrected_image")

        cv2.destroyAllWindows()


def partitioned_affine(crop_img):
    # Partition affine correction
    canvas = np.ones(crop_img.shape, dtype="uint8")
    canvas = list()
    divisor = 2
    h, w, _ = crop_img.shape
    limiters = [(w//divisor)*i for i in range(divisor)]
    limiters.append(w)
    # print(limiters)
    for i in range(len(limiters) - 1):
        start = limiters[i]
        stop = limiters[i+1]
        n_img = crop_img[0:h, start:stop]
        new_h, new_w, _ = n_img.shape
        n_img = affine_correction(n_img)
        # To make them of same size
        n_img = cv2.resize(n_img, (new_w, new_h), interpolation=cv2.INTER_LINEAR)
        canvas.append(n_img)
    return np.concatenate(canvas, axis=1)


if __name__ == "__main__":
    main()