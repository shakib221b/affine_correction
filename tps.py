import cv2
import numpy as np
import random
import json
from argparse import ArgumentParser
from utils import order_points, get_contour_outline, draw_circle, show


def get_args():
    parser = ArgumentParser(description='Apply This Plate Spline to Nameplates')
    parser.add_argument("-i", "--image", required=True, help="input image file")
    parser.add_argument("-a", "--annos", required=True, help="corresponding json annotation file")
    return parser.parse_args()


def main():
    args = get_args()
    img = cv2.imread(args.image)

    with open(args.annos) as f:
        annos = json.load(f)

    objects = annos["objects"]
    for idx, obj in enumerate(objects):
        if obj["classTitle"] != "Name Plate":
            continue
        if len(obj["tags"]) != 0:
            # It's unclear
            continue
        bbox = obj["points"]["exterior"]

        # draw poly in order to get contour of nameplates
        pts = np.array(obj["points"]["exterior"])
        pts = pts.astype(np.int32)
        pts = pts.reshape((-1,1,2))
        img_draw_contour = np.zeros(img.shape).astype(np.uint8)
        cv2.fillPoly(img_draw_contour, [pts], (0, 0, 255))
        # show(img_draw_contour, "poly")

        # Make cv2 contour-like shape
        bbox = np.array(bbox, dtype="int32")
        draw_circle(img.copy(), bbox)
        bbox = np.expand_dims(bbox, 1)

        x, y, w, h = cv2.boundingRect(bbox)
        print(x, y)
        # Draw rectangle on the ROI of image
        show(cv2.rectangle(img.copy(), (x, y), (x+w, y+h), (0, 0, 255), 2), "rect")

        crop_padding = 2 if y is not 0 else 0
        # due to cyllindrical bending roi is taken a bit larger
        crop_img = img[y-crop_padding:y+h+crop_padding, x-crop_padding:x+w+crop_padding]
        crop_img_draw_contour = img_draw_contour[y-crop_padding:y+h+crop_padding, x-crop_padding:x+w+crop_padding]
        show(crop_img_draw_contour, "crop_img")

        c = get_contour_outline(crop_img_draw_contour)
        top_point = np.min(c, axis=0)[1]
        bottom_point = np.max(c, axis=0)[1]
        # print(top_point, bottom_point)
        rect = order_points(c)
        # print(rect)
        # draw_circle(crop_img.copy(), rect)
        tl, tr, br, bl = rect
        mid_y = (min(bl[1], br[1]) + max(tl[1], tr[1]))//2
        print(mid_y)
        # for point in c:
        #     print(point)
        #     break
        pad = 10
        # padding added to avoid accidental inclusion of side edge points
        upper_edge = [
            point for point in c if tl[0]+pad < point[0] < tr[0]-pad and point[1] < mid_y
        ]
        upper_edge = sorted(upper_edge, key=lambda x: x[0])
        
        lower_edge = [
            point for point in c if bl[0]+pad < point[0] < br[0]-pad and point[1] > mid_y
        ]
        lower_edge = sorted(lower_edge, key=lambda x: x[0])

        # number of samples to take
        # caution: too much points generates noisy image
        n_sample = min(50, len(lower_edge), len(upper_edge))

        sampled_upper_edge = [tl, tr]
        # FIXME: we should not use random sampling
        sampled_upper_edge.extend(point for i, point in enumerate(upper_edge) if i%3==0)

        if len(upper_edge) == 0 or len(lower_edge) == 0:
            # FIXME: diagonal nameplate cause this error.
            raise Exception()

        target_upper_edge = np.array(sampled_upper_edge)
        draw_circle(crop_img.copy(), sampled_upper_edge)
        target_upper_edge[:, 1] = top_point

        sampled_lower_edge = [bl, br]
        sampled_lower_edge.extend(point for i, point in enumerate(lower_edge) if i%5==0)

        target_lower_edge = np.array(sampled_lower_edge)
        draw_circle(crop_img.copy(), sampled_lower_edge)
        target_lower_edge[:, 1] = bottom_point

        tps = cv2.createThinPlateSplineShapeTransformer()

        sshape = np.concatenate((sampled_upper_edge, sampled_lower_edge)).astype(np.float32)
        tshape = np.concatenate((target_upper_edge, target_lower_edge)).astype(np.float32)

        sshape = sshape.reshape(1, -1, 2)
        tshape = tshape.reshape(1, -1, 2)
        matches = list()
        for i in range(sshape.shape[1]):
            matches.append(cv2.DMatch(i, i, 0))

        tps.estimateTransformation(tshape,sshape,matches)
        out_img = tps.warpImage(crop_img)

        show(out_img, "output")
        cv2.imwrite(f'out_{idx}.png', out_img)


if __name__ == "__main__":
    main()
